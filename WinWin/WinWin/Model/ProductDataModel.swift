//
//  ProductDataModel.swift
//  WinWin
//
//  Created by Pham Ngoc Nghia on 1/6/16.
//  Copyright © 2016 Pham Ngoc Nghia. All rights reserved.
//

import UIKit

class ProductDataModel: NSObject {
    var productName: String?
    var productPrice: Int?
    var productImage: String?
    
    override init() {
        
    }
    
    init(productName: String, productPrice: Int, productImage: String) {
        super.init()
        self.productName = productName
        self.productPrice = productPrice
        self.productImage = productImage
    }
}
