//
//  UserInfo.swift
//  WinWin
//
//  Created by Pham Ngoc Nghia on 1/21/16.
//  Copyright © 2016 Pham Ngoc Nghia. All rights reserved.
//

import UIKit
class UserInfo: NSObject {

    var username: String?
    var password: String?
    var objectID: String?
    var sessionToken: String?
    
    init(username: String, objectID: String, sessionToken: String) {
        super.init()
        self.username = username
        self.objectID = objectID
        self.sessionToken = sessionToken
    }
    
    func getName() -> String{
        return username!
    }
    
    func getObjectId() -> String {
        return objectID!
    }
    
    func getSessionToken() -> String {
        return sessionToken!
    }
}
