//
//  MainMenuViewController.swift
//  WinWin
//
//  Created by Pham Ngoc Nghia on 1/4/16.
//  Copyright © 2016 Pham Ngoc Nghia. All rights reserved.
//

import UIKit

class MainMenuViewController: UITableViewController {

    @IBOutlet weak var mUserImage: UIImageView!
    @IBOutlet weak var mUserName: UILabel!
    @IBOutlet weak var mUserAddress: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()

        self.mUserImage.layer.cornerRadius = 40
        self.mUserImage.clipsToBounds = true
        self.mUserImage.image = UIImage(named: "iphone.jpg")
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
