//
//  CustomCollectionViewCell.swift
//  WinWin
//
//  Created by Pham Ngoc Nghia on 12/29/15.
//  Copyright © 2015 Pham Ngoc Nghia. All rights reserved.
//

import UIKit

class CustomCollectionViewCell: UICollectionViewCell {
    
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var productName: UILabel!
    @IBOutlet weak var price: UILabel!
    @IBOutlet weak var mAddToCartButton: UIButton!
    
    override init(frame: CGRect){
        super.init(frame: frame)
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
}
