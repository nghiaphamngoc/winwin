//
//  MainCollectionViewController.swift
//  WinWin
//
//  Created by Pham Ngoc Nghia on 12/29/15.
//  Copyright © 2015 Pham Ngoc Nghia. All rights reserved.
//

import UIKit
import BBBadgeBarButtonItem
import Alamofire
import SwiftyJSON
import SDWebImage

let headerGet = ["X-Parse-Application-Id":"HBoeiQz6NK2afDBI77KhVHvv8oecXR70oBeG99DU",
    "X-Parse-REST-API-Key":"JnNgS9vqSJxxlcgl9AvNwCiWLTRyqodKhVllZzLG"]
var listCartProduct = [ProductDataModel]()
private let reuseIdentifier = "Cell"

class MainCollectionViewController: UICollectionViewController {

    @IBOutlet weak var mMenuButton: UIBarButtonItem!
    @IBOutlet weak var mCartButton: UIBarButtonItem!
    
    var navBar = UINavigationBar(frame: CGRectZero)
    let badgeButton = MIBadgeButton(type: UIButtonType.Custom)
    var listProduct = [ProductDataModel]()

    var product = ProductDataModel()
    var isFinish = false
    let progressHUD = ProgressHUD(text: "Loading")
    
    override func viewDidLoad() {
        super.viewDidLoad()

        if self.revealViewController() != nil {
            mMenuButton.target = self.revealViewController()
            mMenuButton.action = "revealToggle:"
            self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        }

        self.collectionView!.registerNib(UINib(nibName: "CustomCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "WinCell")
        
        navigationController?.navigationBar.topItem?.title = "Activity"
        self.view.backgroundColor = UIColor.whiteColor()
        self.view.addSubview(progressHUD)
        self.getListProduct()
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()

    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(true)
        
        self.setupBadgeButton()
        
    }
    
    func setupBadgeButton() {
        if listCartProduct.count > 0 {
            badgeButton.badgeString = "\(listCartProduct.count)"
        } else {
            badgeButton.badgeString = ""
            badgeButton.hidden = true
        }
   
        badgeButton.badgeBackgroundColor = UIColor.blueColor()
        badgeButton.tintColor = UIColor.whiteColor()
        badgeButton.frame = CGRectMake(0, 0, 35, 35)
        badgeButton.badgeEdgeInsets = UIEdgeInsetsMake(10, 0, 0, 5)
        badgeButton.setImage(UIImage(named: "ShopingIcon"), forState: .Normal)
        let rightbarBtn = UIBarButtonItem(customView: badgeButton)
        self.navigationItem.rightBarButtonItem = rightbarBtn

    }
    
    override func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
    
        return 1
    }


    override func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if !isFinish {
            return 0;
        } else {
            return listProduct.count

        }
    }

    override func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier("WinCell", forIndexPath: indexPath) as! CustomCollectionViewCell
        
        let product = listProduct[indexPath.row]

        cell.imageView.sd_setImageWithURL(NSURL(string: product.productImage!))
        cell.productName.text = product.productName
        cell.price.text = "\(product.productPrice!) $"
        cell.layer.cornerRadius = 8
        cell.mAddToCartButton.tag = indexPath.row
        cell.mAddToCartButton.addTarget(self, action: "addToCardButtonClicked:", forControlEvents: UIControlEvents.TouchUpInside)
        return cell
    }

    override func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        if let cell = collectionView.cellForItemAtIndexPath(indexPath){
            product = listProduct[indexPath.row]
            performSegueWithIdentifier("activityShowProductDetail", sender: cell)
        }
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        assert(sender as? UICollectionViewCell != nil, "sender not from activity")
        if let indexPath = self.collectionView?.indexPathForCell(sender as! UICollectionViewCell)
        {
            if segue.identifier == "activityShowProductDetail" {
                let detailVC: ProductDetailsViewController = segue.destinationViewController as! ProductDetailsViewController
                detailVC.product = product
            }
        }
    }
    
    func setNavigationBarTitleText(text: String){
        let titleLabel = UILabel(frame: CGRectMake(0, 0, view.frame.size.width - 120, 44))
        titleLabel.backgroundColor = UIColor.clearColor()
        titleLabel.textColor = UIColor.blueColor()
        titleLabel.numberOfLines = 2
        titleLabel.textAlignment = NSTextAlignment.Center
        titleLabel.text = text.uppercaseString
        self.navBar.topItem?.titleView = titleLabel
    }
    
    func addToCardButtonClicked(sender : UIButton) {
        badgeButton.hidden = false
        listCartProduct.append(listProduct[sender.tag])
        badgeButton.badgeString = "\(listCartProduct.count)"
    }
    
    
    func getListProduct() {
        Alamofire.request(.GET, "https://api.parse.com/1/classes/Product", parameters: ["":""], encoding: .URLEncodedInURL, headers: headerGet).responseJSON { response in
            
            switch response.result {
            case .Success:
               
                let json = JSON(data: response.data!)
                let jsonArray = json["results"].array!
                for object in jsonArray {
                    let productDataModel = ProductDataModel(productName: object["ProductName"].string!, productPrice: (object["ProductPrice"].number as? Int)!, productImage: object["Image"]["url"].string!)
                    self.listProduct.append(productDataModel)
                }
                self.isFinish = true
                self.progressHUD.hide()
                self.collectionView?.reloadData()
                break
                
            case .Failure(let error):
                print("Error while parsing \(error)")
                break
            }
        }
    }
    
}
