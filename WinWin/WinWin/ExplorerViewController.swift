//
//  ExplorerViewController.swift
//  WinWin
//
//  Created by Pham Ngoc Nghia on 12/29/15.
//  Copyright © 2015 Pham Ngoc Nghia. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import SDWebImage

class ExplorerViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet weak var mMenuButton: UIBarButtonItem!
    @IBOutlet weak var mExplorerTableView: UITableView!
    @IBOutlet weak var loadingIndicator: UIActivityIndicatorView!
    
    let badgeButton = MIBadgeButton(type: UIButtonType.Custom)
    var listProduct = [ProductDataModel]()
    var product = ProductDataModel()
    var isFinish = false
    let progressHUD = ProgressHUD(text: "Loading")
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if self.revealViewController() != nil {
            mMenuButton.target = self.revealViewController()
            mMenuButton.action = "revealToggle:"
            self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        }
        
        self.mExplorerTableView.registerNib(UINib(nibName: "ExplorerTableViewCell", bundle: nil), forCellReuseIdentifier: "ExplorerCell")
        self.view.addSubview(progressHUD)
        self.getListProduct()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
 
    override func viewWillAppear(animated: Bool) {
        self.setupBadgeButton()
    }
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if !isFinish {
            return 0;
        }else {
            return listProduct.count
        }
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cellIdentifier = "ExplorerCell"
        let cell = tableView.dequeueReusableCellWithIdentifier(cellIdentifier, forIndexPath: indexPath) as! ExplorerTableViewCell
        let product = listProduct[indexPath.row]
        cell.mProductImage.sd_setImageWithURL(NSURL(string: product.productImage!))
        cell.mProductName.text = product.productName
        cell.mPrice.text = "\(product.productPrice!) $"
        cell.mAddToCartButton.tag = indexPath.row
        cell.mAddToCartButton.addTarget(self, action: "addToCardButtonClicked:", forControlEvents: UIControlEvents.TouchUpInside)
        return cell
    }
    

    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        if let cell = mExplorerTableView.cellForRowAtIndexPath(indexPath){
            mExplorerTableView.deselectRowAtIndexPath(indexPath, animated: true)
            product = listProduct[indexPath.row]
            performSegueWithIdentifier("ExplorerProductDetails", sender: cell)
        }

    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if let indexPath = self.mExplorerTableView.indexPathForCell(sender as! UITableViewCell)
        {
            if segue.identifier == "ExplorerProductDetails" {
                let detailVC: ProductDetailsViewController = segue.destinationViewController as! ProductDetailsViewController
                detailVC.product = product
            }
        }
    }
    
    func getListProduct() {
        Alamofire.request(.GET, "https://api.parse.com/1/classes/Product", parameters: ["":""], encoding: .URLEncodedInURL, headers: headerGet).responseJSON { response in
            
            switch response.result {
            case .Success:
                
                let json = JSON(data: response.data!)
                let jsonArray = json["results"].array!
                for object in jsonArray {
                    let productDataModel = ProductDataModel(productName: object["ProductName"].string!, productPrice: (object["ProductPrice"].number as? Int)!, productImage: object["Image"]["url"].string!)
                    self.listProduct.append(productDataModel)
                }
                self.isFinish = true
                 self.progressHUD.hide()
                self.mExplorerTableView?.reloadData()
                break
                
            case .Failure(let error):
                print("Error while parsing \(error)")
                break
            }
        }
    }

    func setupBadgeButton() {
        if listCartProduct.count > 0 {
            badgeButton.badgeString = "\(listCartProduct.count)"
        } else {
            badgeButton.badgeString = ""
            badgeButton.hidden = true
        }
        
        badgeButton.badgeBackgroundColor = UIColor.blueColor()
        badgeButton.tintColor = UIColor.whiteColor()
        badgeButton.frame = CGRectMake(0, 0, 35, 35)
        badgeButton.badgeEdgeInsets = UIEdgeInsetsMake(10, 0, 0, 5)
        badgeButton.setImage(UIImage(named: "ShopingIcon"), forState: .Normal)
        let rightbarBtn = UIBarButtonItem(customView: badgeButton)
        self.navigationItem.rightBarButtonItem = rightbarBtn
        
    }

    func addToCardButtonClicked(sender : UIButton) {
        badgeButton.hidden = false
        listCartProduct.append(listProduct[sender.tag])
        badgeButton.badgeString = "\(listCartProduct.count)"
    }
}
