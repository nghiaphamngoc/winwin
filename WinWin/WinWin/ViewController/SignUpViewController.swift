//
//  SignUpViewController.swift
//  WinWin
//
//  Created by Pham Ngoc Nghia on 1/18/16.
//  Copyright © 2016 Pham Ngoc Nghia. All rights reserved.
//

import UIKit
import Alamofire
import SDCAlertView

var headerSignUp = ["X-Parse-Application-ID":"HBoeiQz6NK2afDBI77KhVHvv8oecXR70oBeG99DU",
    "X-Parse-REST-API-Key":"JnNgS9vqSJxxlcgl9AvNwCiWLTRyqodKhVllZzLG",
    "X-Parse-Revocable-Session":"1", "Content-Type":"application/json"]
class SignUpViewController: UIViewController {
    
    @IBOutlet weak var mUserNameTextField: UITextField!
    @IBOutlet weak var mPasswordTextField: UITextField!
    @IBOutlet weak var mFistName: UITextField!
    @IBOutlet weak var mLastName: UITextField!
    @IBOutlet weak var mEmailAddress: UITextField!
    

    override func viewDidLoad() {
        self.navigationItem.title = "Create Account"
        
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: "dismissKeyboard")
        view.addGestureRecognizer(tap)
    }
    @IBAction func SignUp(sender: AnyObject) {
        
        let param = ["username": mUserNameTextField.text!,
            "password": mPasswordTextField.text!,
            "firstname": "\(mFistName.text!)",
            "lastname": "\(mLastName.text!)",
        "email": "\(mEmailAddress.text!)"]
        
        if self.checkInputTextFiled() {
            
            Alamofire.request(.POST, "https://api.parse.com/1/users",
                parameters: param, encoding: .JSON, headers: headerSignUp).responseJSON { response in
                    print(response.request)  // original URL request
                    print(response.response) // URL response
                    print(response.data)     // server data
                    print(response.result)   // result of response serialization
                    
                    if let JSON = response.result.value {
                        print("JSON: \(JSON)")
                    }
                    
                    switch response.result {
                        
                    case .Success:
                        print("Validation Successful")
                        
                        let alert = AlertController(title: "Success", message: "Account create successful", preferredStyle: .Alert)
                        alert.addAction(AlertAction(title: "OK", style: .Preferred, handler: { (AlertAction) -> Void in
                            self.navigationController?.popViewControllerAnimated(true)
                        }))
                        alert.addAction(AlertAction(title: "Cancel", style: .Default))
                        alert.present()
                        
                    case .Failure(let error):
                        print(error)
                    }
            }
        } else {
            
            let alert = UIAlertController(title: "Warning", message: "User Name and Password can not empty", preferredStyle: UIAlertControllerStyle.Alert)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: nil))
            self.presentViewController(alert, animated: true, completion: nil)

        }
    }
    

    func textFieldDidBeginEditing(textField: UITextField) {

    }
    func textFieldDidEndEditing(textField: UITextField) {

    }
    
    
    func checkInputTextFiled() -> Bool{
        if ((self.mUserNameTextField.text?.isEmpty != false) && (self.mPasswordTextField.text?.isEmpty != false)) {
            return false
        }else {
            return true
        }
    }
    
    func dismissKeyboard() {
        //Causes the view (or one of its embedded text fields) to resign the first responder status.
        view.endEditing(true)
    }

}
