//
//  Cache.swift
//  WinWin
//
//  Created by Pham Ngoc Nghia on 1/26/16.
//  Copyright © 2016 Pham Ngoc Nghia. All rights reserved.
//

import UIKit

class Cache: NSObject {

    override init() {

    }
    
    func SaveProductToCache(product: [ProductDataModel]) {
        let placesData = NSKeyedArchiver.archivedDataWithRootObject(product)
        NSUserDefaults.standardUserDefaults().setObject(placesData, forKey: "Cart")
        print("Array Saved \(placesData)")
    }
    
    func LoadProductFromCache() -> [ProductDataModel] {
        
        let cartData = NSUserDefaults.standardUserDefaults().objectForKey("Cart") as? NSData
        
        if let cartData = cartData {
            let cartArray = NSKeyedUnarchiver.unarchiveObjectWithData(cartData) as? [ProductDataModel]
            print("Loaded Array \(cartArray)")
            return cartArray!
        } else{
            return []
        }
   
    }
}
