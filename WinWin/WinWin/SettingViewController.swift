//
//  SettingViewController.swift
//  WinWin
//
//  Created by Pham Ngoc Nghia on 1/5/16.
//  Copyright © 2016 Pham Ngoc Nghia. All rights reserved.
//

import UIKit

class SettingViewController: UITableViewController {

    @IBOutlet weak var mMenuButton: UIBarButtonItem!
    override func viewDidLoad() {
        super.viewDidLoad()

        
        if self.revealViewController() != nil {
            mMenuButton.target = self.revealViewController()
            mMenuButton.action = "revealToggle:"
            self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        }

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
//    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
//        return 1
//    }
//    
//    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        return 4
//    }
//    
//    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
//  
//        let cell = tableView.dequeueReusableCellWithIdentifier("SettingCell", forIndexPath: indexPath) as UITableViewCell
//        
//        return cell
//    }

}
