//
//  CartCollectionViewCell.swift
//  WinWin
//
//  Created by Pham Ngoc Nghia on 1/4/16.
//  Copyright © 2016 Pham Ngoc Nghia. All rights reserved.
//

import UIKit

class CartCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var mImageVIew: UIImageView!
    @IBOutlet weak var mPrice: UILabel!
    @IBOutlet weak var mProductName: UILabel!
    @IBOutlet weak var mRemoveFromCartBtn: UIButton!
    
    
    override func awakeFromNib() {
        
        super.awakeFromNib()
        
    }
    
    override init(frame: CGRect){
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }

}
