//
//  CartCheckOutCollectionViewCell.swift
//  WinWin
//
//  Created by Pham Ngoc Nghia on 1/4/16.
//  Copyright © 2016 Pham Ngoc Nghia. All rights reserved.
//

import UIKit

class CartCheckOutCollectionViewCell: UICollectionViewCell {

    
    @IBOutlet weak var mTotalPrice: UILabel!
    @IBOutlet weak var mOderNowBtn: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
