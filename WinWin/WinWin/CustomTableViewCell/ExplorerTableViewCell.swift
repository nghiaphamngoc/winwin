//
//  ExplorerTableViewCell.swift
//  WinWin
//
//  Created by Pham Ngoc Nghia on 12/30/15.
//  Copyright © 2015 Pham Ngoc Nghia. All rights reserved.
//

import UIKit

class ExplorerTableViewCell: UITableViewCell {

    @IBOutlet weak var mProductImage: UIImageView!
    @IBOutlet weak var mProductName: UILabel!
    @IBOutlet weak var mPrice: UILabel!
    @IBOutlet weak var mAddToCartButton: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.layoutMargins = UIEdgeInsets(top: 20, left: 20, bottom: 20, right: 20)
        
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
