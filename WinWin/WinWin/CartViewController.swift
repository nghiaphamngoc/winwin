//
//  CartViewController.swift
//  WinWin
//
//  Created by Pham Ngoc Nghia on 12/30/15.
//  Copyright © 2015 Pham Ngoc Nghia. All rights reserved.
//

import UIKit
import SDCAlertView

class CartViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {

    @IBOutlet weak var mMenuButton: UIBarButtonItem!
    
    @IBOutlet weak var mCollectionView: UICollectionView!
    
    let secsionInset = UIEdgeInsetsMake(10,10,10,10)
    
    override func viewDidLoad() {
        super.viewDidLoad()

        if self.revealViewController() != nil {
            mMenuButton.target = self.revealViewController()
            mMenuButton.action = "revealToggle:"
            self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        }
        
        self.mCollectionView!.registerNib(UINib(nibName: "CartCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "CartCell")
        
        self.mCollectionView!.registerNib(UINib(nibName: "CartCheckOutCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "CartCheckOutCell")
        
        let mFlowLayout = UICollectionViewFlowLayout()
        mFlowLayout.sectionInset = UIEdgeInsetsMake(20, 20, 20, 20)
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 2
    }
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if section == 0
        {
            return listCartProduct.count
        }
        return 1
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        
        if indexPath.section == 0
        {
            let cell = collectionView.dequeueReusableCellWithReuseIdentifier("CartCell", forIndexPath: indexPath) as! CartCollectionViewCell
            let cart = listCartProduct[indexPath.row]
            cell.mImageVIew.sd_setImageWithURL(NSURL(string: cart.productImage!))
            cell.mProductName.text = cart.productName
            cell.mPrice.text = "\(cart.productPrice!)"
            cell.mRemoveFromCartBtn.tag = indexPath.row
            cell.mRemoveFromCartBtn.addTarget(self, action: "removeFromCartButtonClicked:", forControlEvents: UIControlEvents.TouchUpInside)
            cell.layer.cornerRadius = 8
            
            return cell
        }
        else
        {
          let cellCheckOut = collectionView.dequeueReusableCellWithReuseIdentifier("CartCheckOutCell", forIndexPath: indexPath) as! CartCheckOutCollectionViewCell
            cellCheckOut.mTotalPrice.text = "\(getTotalPrice())"
            cellCheckOut.mOderNowBtn.addTarget(self, action: "oderNowButtonClicked:", forControlEvents: UIControlEvents.TouchUpInside)
            return cellCheckOut
        }
        
    }
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        if indexPath.section == 1
        {
            return CGSizeMake(self.mCollectionView.frame.size.width, 150)
        }
        return CGSizeMake(self.mCollectionView.frame.size.width/2 - 20, 100)
    }
    
    
    func setCollectionViewLayout(layout: UICollectionViewLayout, animated: Bool, completion: ((Bool) -> Void)?)
    {
        
    }
    
    func getTotalPrice() -> Int {
        var total = 0
        for product in listCartProduct {
            total += product.productPrice!
        }
        return total
    }
    
    func removeFromCartButtonClicked(sender : UIButton) {
        listCartProduct.removeAtIndex(sender.tag)
        self.mCollectionView.reloadData()
    }
    
    func oderNowButtonClicked(sender: UIButton) {
        
        let alert = AlertController(title: "Payment", message: "Payment Success", preferredStyle: .Alert)
        alert.addAction(AlertAction(title: "OK", style: .Preferred, handler: { (AlertAction) -> Void in
            listCartProduct.removeAll()
            self.mCollectionView.reloadData()
        }))
        alert.addAction(AlertAction(title: "Cancel", style: .Default))
        alert.present()
    }
}
