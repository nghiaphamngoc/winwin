//
//  LogInViewController.swift
//  WinWin
//
//  Created by PhamNgocNghia on 1/5/16.
//  Copyright © 2016 Pham Ngoc Nghia. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class LogInViewController: UIViewController {

    @IBOutlet weak var mMenuButton: UIBarButtonItem!
    @IBOutlet weak var mUserNameTextField: UITextField!
    @IBOutlet weak var mPasswordTextField: UITextField!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        if self.revealViewController() != nil {
            mMenuButton.target = self.revealViewController()
            mMenuButton.action = "revealToggle:"
            self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        }

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func SignIn(sender: AnyObject) {
        
        let header = ["X-Parse-Application-Id":"HBoeiQz6NK2afDBI77KhVHvv8oecXR70oBeG99DU",
                        "X-Parse-REST-API-Key":"JnNgS9vqSJxxlcgl9AvNwCiWLTRyqodKhVllZzLG",
                        "X-Parse-Revocable-Session":"1"]

        let param = ["username":self.mUserNameTextField.text!, "password":self.mPasswordTextField.text!]
        
        let url = "https://api.parse.com/1/login"
        Alamofire.request(.GET, url, parameters: param, encoding: .URL, headers: header).responseJSON { response in
            print(response.request)  // original URL request
            print(response.response) // URL response
            print(response.data)     // server data
            print(response.result)   // result of response serialization
            
            if let JSON = response.result.value {
                print("JSON: \(JSON)")
            }
            
            switch response.result {
            case .Success:
                let jsonResult = JSON(data: response.data!)
                let error = jsonResult["error"].stringValue
       
                if error.characters.count > 0{
                    //show error banner
                    let alert = UIAlertController(title: "Alert", message: "\(jsonResult["error"])", preferredStyle: UIAlertControllerStyle.Alert)
                    alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: nil))
                    self.presentViewController(alert, animated: true, completion: nil)
                    
                } else {
                    //saved to properties
                    UserInfo.init(username: jsonResult["username"].stringValue, objectID: jsonResult["objectId"].stringValue, sessionToken: jsonResult["sessionToken"].stringValue)
                    
                    //saved to userdefaults
                    let defaults = NSUserDefaults.standardUserDefaults()
                    defaults.setObject("\(jsonResult["username"])", forKey: "username")
                    defaults.setObject("\(jsonResult["sessionToken"])", forKey: "sessionToken")
                    defaults.setObject("\(jsonResult["objectId"])", forKey: "objectId")
                    
                    let alert = UIAlertController(title: "Welcome", message: "Login Successfull", preferredStyle: UIAlertControllerStyle.Alert)
                    alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: nil))
                    self.presentViewController(alert, animated: true, completion: nil)

                        }
            case .Failure(let error):
                let alert = UIAlertController(title: "Welcome", message: "\(error)", preferredStyle: UIAlertControllerStyle.Alert)
                alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: nil))
                self.presentViewController(alert, animated: true, completion: nil)
            }
        }
    }
    
    @IBAction func SignUp(sender: AnyObject) {
        let signUpViewController = self.storyboard?.instantiateViewControllerWithIdentifier("CreateAccount") as? SignUpViewController
        self.navigationController?.pushViewController(signUpViewController!, animated: true)
    }
}
