//
//  ProductDetailsViewController.swift
//  WinWin
//
//  Created by Pham Ngoc Nghia on 1/5/16.
//  Copyright © 2016 Pham Ngoc Nghia. All rights reserved.
//

import UIKit
import SDWebImage

class ProductDetailsViewController: UIViewController {

    @IBOutlet weak var mProductImage: UIImageView!
    @IBOutlet weak var mProductName: UILabel!
    @IBOutlet weak var mProductSize: UILabel!
    @IBOutlet weak var mProductColor: UILabel!
    @IBOutlet weak var mProductPrice: UILabel!
    @IBOutlet weak var mStatusLike: UIButton!
    @IBOutlet weak var mStatusComment: UIButton!
    @IBOutlet weak var mStatusForward: UIButton!
    
    @IBOutlet weak var viewPrice: UIView!
    @IBOutlet weak var viewProductName: UIView!
    @IBOutlet weak var viewLike: UIView!
    @IBOutlet weak var viewSelectItem: UIView!
    @IBOutlet weak var viewComment: UIView!
    @IBOutlet weak var viewForward: UIView!
    
    var product = ProductDataModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(animated: Bool) {
        mProductName.text = self.product.productName
        mProductPrice.text = "\(self.product.productPrice!) $"
        mProductImage.sd_setImageWithURL(NSURL(string: self.product.productImage!))
        setupView()
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    func setupView() {
        viewPrice.layer.cornerRadius = 8
        viewProductName.layer.cornerRadius = 8
        viewLike.layer.cornerRadius = 8
        viewSelectItem.layer.cornerRadius = 8
        viewComment.layer.cornerRadius = 8
        viewForward.layer.cornerRadius = 8
    }
}
